Source: golang-github-alessio-shellescape
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Bradford D. Boyle <bradford.d.boyle@gmail.com>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/shellescape
Vcs-Git: https://salsa.debian.org/go-team/packages/shellescape.git
Homepage: https://github.com/alessio/shellescape
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/alessio/shellescape

Package: escargs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Print shell-escape lines read from standard input
 escargs reads lines from the standard input and prints shell-escaped versions.
 Unlinke xargs, blank lines on the standard input are not discarded.


Package: golang-github-alessio-shellescape-dev
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Escape arbitrary strings for use as command line arguments (program)
 Escape arbitrary strings for safe use as command line arguments. This package
 provides the shellescape.Quote() function that returns a shell-escaped copy of
 a string. This functionality could be helpful in those cases where it is known
 that the output of a Go program will be appended to/used in the context of
 shell programs' command line arguments.
 .
 This work was inspired by the Python original package shellescape
 (https://pypi.python.org/pypi/shellescape).
